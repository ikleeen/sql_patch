# SQL patch for 9 task

Describes a stored procedure and patch for product table updates where records with current_owner_id is not correct. Patch only updates the current_owner_id value with presented id (not created if id is not present)

### Apply patch
-------------
```sh
    $ mysql -u root -p < assignment.sql
```
