
USE assignment

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sort_product` ()  BEGIN
SELECT * FROM assignment ORDER BY product_id, action, ts;
END$$

DELIMITER ;

UPDATE product INNER JOIN (SELECT m1.*
FROM assignment m1 LEFT JOIN assignment m2
 ON (m1.product_id = m2.product_id AND m1.id < m2.id)
WHERE m2.product_id IS NULL) s
    ON product.id = s.product_id
SET product.current_owner_id = s.participant_id